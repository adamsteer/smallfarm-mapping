# Making useful maps with a small drone and QGIS

### Dr Adam Steer

This repository contains a set of tools and workflows for mapping with a "prosumer" drone and open source tools. It can be delivered as an in-office or in-field workshop, or you can go through it at your own pace. Paid support is available - get in touch for options!

It is aiming to help:
- geospatial consultants
- exploration geologists
- landowners
- conservation groups
- planners
- permaculture designers
- builders
- researchers

Depending on funding and demand it will grow to include deeper concepts like using high accuracy GNSS and field data collection devices. 

For now, it focuses on the user case "I have a small drone and a computer and want to know things" - and also details about how and where problems creep in. The aim is to help you understand what is possible for you!

It is set up to run each step at a time. Yes! It could all be more automated. Doing so robs us of the ability to inspect results at each step, and identify process issues before they become embedded in a final product.

## Setting up

This whole workflow is Ubuntu-based. If you use Windows, most of what happens here can be done with the Conda package manager (even if you don't have admin rights). Please remember to modify path conventions (`/' and '\' etc). If you want to contribute better Windows instructions reach out. If you want to fund a fully Windows based version (and can provide / loan hardware with Windows installed) please reach out!

### Software

Requires QGIS > 3.3, docker, and optionally Miniconda (or Mamba).

#### Ubuntu

Install docker and set permissions so that you don't need to be `sudo` to run it, then install OpenDroneMap:

`docker pull opendronemap/odm`

Alternately, install WebODM:

https://github.com/OpenDroneMap/WebODM/#getting-started

WebODM users should inspect ODM run scripts and configure options accordingly. 

**optionally** install CloudCompare. You may need to build CloudCompare on Linux from source to get ASPRS LAS file format support.

Install core parts - QGIS, GDAL and PDAL:

`sudo apt install qgis gdal-bin pdal`

It may be worth setting up a miniconda (or mamba) environment for your work. I use different virtual environments defined in Mamba for various tasks, rather than try to make one giant toolbox for all the things.

Mamba instructions: https://mamba.readthedocs.io/en/latest/installation/mamba-installation.html

Miniconda instructions: https://docs.conda.io/projects/miniconda/en/latest/miniconda-install.html

Deploying geospatial tooling using virtual environments is a huge topic all of its own. Have a read through this: https://pygis.io/docs/b_conda_started2.html


#### Windows

In windows, the best path I've come across is to start with the Miniconda package manager. Windows installation instructions are here:

https://docs.conda.io/projects/miniconda/en/latest/miniconda-install.html

**Note** installing Miniconda for a single user may circumvent enterprise software systems, use at your own risk or in consultation with your workplace.

Create an environment with the tools needed. If you already have QGIS do:

`conda create -n odmworkflow pdal gdal -c conda-forge`

If you don't have QGIS already, add it to the conda create command:

`conda create -n odmworkflow pdal gdal qgis -c conda-forge`

Install WebODM - fastest path is to buy the (very cheap) installer:

https://www.opendronemap.org/webodm/download/#installer


In your conda terminal, use:

`conda activate odmworkflow`

...and then QGIS, pdal and gdal can be run from the Conda command line.


## Flying for data collection

There are many resources for this. OpenDroneMap's guides are great: https://docs.opendronemap.org/tutorials/#creating-digital-elevation-models

If you want to break all the rules and still make maps, some experiences from high latitude flights mapping drifting ice (no static ground control possible) is here: https://gitlab.com/adamsteer/aen/-/tree/main/anafi-operations


## Organising imagery

In a location with *plenty* of storage space, set up a folder for your project - give it a name that helps you understand what is inside it:

`/path/to/farm-map-17-may-2023/opendronemap`

Move to the folder and create an `images` folder inside it, forming:

`/path/to/farm-map-17-may-2023/opendronemap/images`

Place your drone mapping JPEGs into the images folder. Your overall projects folder is a great place to store other things like RAW imagery, trajectory logs etc also.

**note** WebODM uses a separate imagery and project storage structure - the way of work described here is for running ODM on the command line.


## Running OpenDroneMap on the command line

Inspect the options in the provided OpenDroneMap run script. If you're happy, copy it to your newly minted `opendronemap` folder and then:

`sh ./odm-runscripts/runodm-highquality.sh`

...and wait. 


## Running WebODM

...

## Point cloud manipulation

Generating terrain data is the first step in an analytical process. Now we need to use the data to do things. If you've used PDAL for a while you might note that some of these pipelines are shorter than they might have been in the past. PDAL's introduction of a `where` syntax and `filters.expression` a little while ago mean we can reduce the number of stages needed.

### Segment ground

You can ask OpenDroneMap to segment ground using PDAL's SMRF filter. I tend to do that, and then run a ground segmentation again using a Cloth Sampling Filter (CSF). I prefer CSF filter results, although it can take some work to get the filter tuned just right.

[clothsample.json](./pdal-pipelines/clothsample.json) runs the cloth sample filter using a fairly high resolution, reasonably rigid simulated cloth - it delivered decent results in 10 cm elevation data over some hilly land:

`pdal pipeline pdal-pipelines/clothsample.json --readers.las.filename=./odm_georeferenced_model.laz --writers.las.filename=./csf-groundsegmented.laz -v 8`

[hag_delaunay.json](./pdal-pipelines/hag_delaunay.json) is used to compute height above ground using Delaunay triangulation between a set of nearby ground points to infer the height of 'ground' under non-ground points:

`pdal pipeline pdal-pipelines/hag_delaunay.json --readers.las.filename=csf-groundsegmented.laz --writers.copc.filename=csf-groundsegmented-HAG.copc.laz -v 8`

Note that this will add an extra dimension `heightAboveGround`. This is fine for LAS 1.4 / COPC, take care if you are using older las specifications.

The pipeline [csf-hag_delaunay.json](./pdal-pipelines/csf-hag_delaunay.json) does a few steps - noise filtering then a CSF ground segmentation then computing height above ground:

`pdal pipeline pdal-pipelines/csf-hag_delaunay.json --readers.las.filename=odm_georeferenced_model.laz --writers.copc.filename=csf-groundsegmented-HAG.copc.laz -v 8`


### Separate ground and nonground

First write out only ground points (`Classification` is `2`). Noise labelled points are automatically excluded.

`pdal pipeline pdal-pipelines/onlyground.json --readers.las.filename=csf-groundsegmented.laz --writers.las.filename=onlyground.laz -v 8 `

Then write out any points not labelled as ground. In this pipeline we explicitly exclude noise points:

`pdal pipeline pdal-pipelines/noground-nonoise.json --readers.las.filename=csf-groundsegmented.laz --writers.las.filename=notground.laz -v 8 `

### Using only ground points write a DTM and then fill voids

Rasterise the ground points to an 0.2m resolution geotiff.

`pdal pipeline pdal-pipelines/dtmify.json --readers.las.filename=onlyground.laz --writers.gdal.resolution=0.2 v 8 `

Fill voids by interpolation of edge values, then smoothing. This assumes you've only written one band (`output_type`) to the raster. It uses 3 smoothing iterations. Note that very large voids may not be completely filled in high resolution rasters.

`gdal_fillnodata.py -si 3 -b 1 dtm.tiff dtm-filled.tiff`

Here's how to write out a 20 cm resolution raster of only nonground objects, where points are at least 1 metre off the ground:

`pdal pipeline pdal-pipelines/rasterise-hag.json --readers.las.filename=csf-groundsegmented-HAG.copc.laz --writers.gdal.resolution=0.2 v 8 `


### Generate point features for nonground objects

*Experimental* - aiming to use point neighbourhood attributes to help segment trees, buildings, other things:

`pdal pipeline pdal-pipelines/covfeatures.json --readers.las.filename=notground.laz --writers.las.filename=covfeatures-rank-coplanar-noground.laz -v 8`

The same approach can be applied to `ground` features or surface models to segment different terrain types - it has been used to identify sea ice ridges and deformed regions, allowing segmentation of flat / undeformed and deformed sea ice: https://doi.org/10.6084/m9.figshare.19923116.v1

# QGIS components

### Startup

Add the ODM orthophoto and the filled DTM (`dtm-filled.tiff`) to the project. If you want to check out the data as an apriori view, drag in the COPC file (`odm_georeferenced.copc.laz`) and explore it in QGIS 3D map view

### Fill sinks in DTM

Use `SAGA` `Fill sinks (Wang & Lui)` to eliminate sinks in the DTM. This helps to ensure we're letting water flow downhill rather than being caught in artefactual terrain. 

Quite often photogrammetric products are 'grass elevation models' rather than actual surface. Smoothing, denoising and sink filling help us get a little closer to underlying terrain - although we need to consider that we've lost detail and introduced new artefacts to our analysis.

### Run hydrological analysis

[TBD]

### Visualise flow

[TBD]


## Support
Feel free to lodge issues here. Note that response to issues depends on funding.

## Project status
Funding contingent. Feel free to add resources.
