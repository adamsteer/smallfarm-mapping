#!/bin/bash
# A helper script to run Opendronemap (https://opendronemap.org) on images from
# the ANAFI thermal RPAS - to generate orthophotos and a representation of terrain
# for sea ice sampling sites

docker run -ti --rm -v $(pwd):/datasets/code opendronemap/odm \
 --project-path /datasets \
 --pc-quality high \
 --feature-quality high \
 --matcher-neighbors 16 \
 --min-num-features 40000 \
 --gps-accuracy 2.0 \
 --use-3dmesh \
 --dem-resolution 5 \
 --orthophoto-resolution 2 \
 --cog \
 --orthophoto-png \
 --dsm \
 --pc-filter 1.5 \
 --pc-sample 0.01 \
 --pc-copc \
 --mesh-size 4000000 \
 --max-concurrency 4 \
 --rerun-all \
 --optimize-disk-space 

